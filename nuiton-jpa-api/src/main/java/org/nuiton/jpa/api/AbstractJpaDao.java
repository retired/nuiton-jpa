package org.nuiton.jpa.api;

/*
 * #%L
 * Nuiton Jpa :: API
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This abstract class gather all code which is common to all daos for all entities.
 * <p/>
 * Code included is common implementations of {@link JpaDao} interface and there is
 * also commun helpers methods for implementation business-specific operations
 * exposed as protected.
 *
 * @author bleny <leny@codelutin.com>
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractJpaDao<E extends JpaEntity> implements JpaDao<E> {

    protected EntityManager entityManager;

    public AbstractJpaDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected abstract Class<E> getEntityClass();

    @Override
    public E findById(String id) {
        E entity = entityManager.find(getEntityClass(), id);
        return entity;
    }

    @Override
    public List<E> findAll() {
        String simpleName = getEntityClass().getSimpleName();
        TypedQuery<E> query = entityManager.createQuery("from " + simpleName, getEntityClass());
        return query.getResultList();
    }

    @Override
    public void persist(E entity) {
        entityManager.persist(entity);
    }

    @Override
    public E merge(E entity) {
        E merge = entityManager.merge(entity);
        return merge;
    }

    @Override
    public void remove(E entity) {
        entityManager.remove(entity);
    }

    @Override
    public boolean contains(E entity) {
        return entityManager.contains(entity);
    }

    public E findByProperty(String propertyName, Object value) {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, value);
        E result = findByProperties(properties);
        return result;
    }

    public List<E> findAllByProperty(String propertyName, Object value) {
        TypedQuery<E> query = createQuery(propertyName, value);
        List<E> result = findAll(query);
        return result;
    }

    public List<E> findAllByProperties(Map<String, Object> properties) {
        TypedQuery<E> query = createQuery(properties);
        List<E> results = findAll(query);
        return results;
    }

    public E findByProperties(Map<String, Object> properties) {
        TypedQuery<E> query = createQuery(properties);
        E result = findAnyOrNull(query);
        return result;
    }

    public E findContains(String propertyName,
                          Object property) {
        TypedQuery<E> query = createQuery("From " + getEntityClass().getSimpleName() + " Where " + propertyName + " In elements(:K)");
        query.setParameter("K", property);
        return findAnyOrNull(query);
    }

    public List<E> findAllContains(String propertyName,
                                   Object property) {
        TypedQuery<E> query = createQuery("From " + getEntityClass().getSimpleName() + " Where " + propertyName + " In elements(:K)");
        query.setParameter("K", property);
        return findAll(query);
    }

    @Override
    public E newInstance() {
        E newInstance;
        try {
            newInstance = getEntityClass().newInstance();
            // TODO brendan 24/05/13 proper exception management
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return newInstance;
    }

    protected TypedQuery<E> createQuery(String propertyName,
                                        Object propertyValue,
                                        Object... others) {

        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, propertyValue);
        Object name = null;
        for (int i = 0; i < others.length; ) {
            try {
                name = others[i++];
                propertyValue = others[i++];
                properties.put((String) name, propertyValue);
            } catch (ClassCastException eee) {
                throw new IllegalArgumentException(
                        "Les noms des propriétés doivent être des chaines et " +
                        "non pas " + propertyName.getClass().getName(),
                        eee);
            } catch (ArrayIndexOutOfBoundsException eee) {
                throw new IllegalArgumentException(
                        "Le nombre d'argument n'est pas un nombre pair: "
                        + (others.length + 2)
                        + " La dernière propriété était: " + name, eee);
            }
        }
        return createQuery(properties);
    }

    protected TypedQuery<E> createQuery(Map<String, Object> properties) {
        return createQuery(getEntityClass(), properties);
    }

    protected <E> TypedQuery<E> createQuery(Class<E> type, String hql) {
        TypedQuery<E> query = entityManager.createQuery(hql, type);
        return query;
    }

    protected <E> TypedQuery<E> createQuery(Class<E> type, String propertyName,
                                            Object propertyValue,
                                            Object... others) {

        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(propertyName, propertyValue);
        Object name = null;
        for (int i = 0; i < others.length; ) {
            try {
                name = others[i++];
                propertyValue = others[i++];
                properties.put((String) name, propertyValue);
            } catch (ClassCastException eee) {
                throw new IllegalArgumentException(
                        "Les noms des propriétés doivent être des chaines et " +
                        "non pas " + propertyName.getClass().getName(),
                        eee);
            } catch (ArrayIndexOutOfBoundsException eee) {
                throw new IllegalArgumentException(
                        "Le nombre d'argument n'est pas un nombre pair: "
                        + (others.length + 2)
                        + " La dernière propriété était: " + name, eee);
            }
        }
        return createQuery(type, properties);
    }

    protected <E> TypedQuery<E> createQuery(Class<E> type, Map<String, Object> properties) {
        List<String> whereClauses = new LinkedList<String>();
        for (Map.Entry<String, Object> property : properties.entrySet()) {
            String propertyName = property.getKey();
            Object propertyValue = property.getValue();
            if (propertyValue == null) {
                whereClauses.add(propertyName + " is null");
            } else {
                whereClauses.add(propertyName + " = :" + propertyName);
            }
        }
        String hql = "from " + getEntityClass().getSimpleName() + " where " + StringUtils.join(whereClauses, " and ");
        TypedQuery<E> query = createQuery(type, hql);
        for (Map.Entry<String, Object> property : properties.entrySet()) {
            Object propertyValue = property.getValue();
            if (propertyValue != null) {
                query.setParameter(property.getKey(), propertyValue);
            }
        }
        return query;
    }

    protected TypedQuery<E> createQuery(String hql) {
        TypedQuery<E> query = entityManager.createQuery(hql, getEntityClass());
        return query;
    }

    protected List<E> findAll(TypedQuery<E> query) {
        return query.getResultList();
    }

    protected E findUnique(TypedQuery<E> query) {
        return query.getSingleResult();
    }

    protected E findAnyOrNull(TypedQuery<E> query) {
        List<E> all = findAll(query);
        E onlyElement = null;
        if (!all.isEmpty()) {
            onlyElement = all.get(0);
        }
        return onlyElement;
    }

    protected E findUniqueOrNull(TypedQuery<E> query) {
        List<E> all = findAll(query);
        E onlyElement = null;
        if (!all.isEmpty()) {
            if (all.size() > 1) {
                throw new IllegalStateException(
                        "multiple results for query = " + query
                        + " results = " + all);
            }
            onlyElement = all.get(0);
        }
        return onlyElement;
    }

}
