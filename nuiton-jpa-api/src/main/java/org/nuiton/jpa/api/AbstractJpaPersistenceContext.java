package org.nuiton.jpa.api;

/*
 * #%L
 * Nuiton Jpa :: API
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * TODO
 *
 * @author bleny <leny@codelutin.com>
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public class AbstractJpaPersistenceContext implements JpaPersistenceContext {

    protected EntityManager entityManager;

    protected EntityTransaction entityTransaction;

    public AbstractJpaPersistenceContext(EntityManager entityManager) {
        this(null, entityManager);
    }

    public AbstractJpaPersistenceContext(JpaEntityIdFactory jpaEntityIdFactory,
                                         EntityManager entityManager) {
        if (jpaEntityIdFactory != null) {
            JpaEntityIdFactoryResolver.setFactory(jpaEntityIdFactory);
        }
        this.entityManager = entityManager;
        entityTransaction = entityManager.getTransaction();
        if (!entityTransaction.isActive()) {
            entityTransaction.begin();
        }
    }

    @Override
    public void commit() {
        entityTransaction.commit();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
    }

    @Override
    public void rollback() {
        entityTransaction.rollback();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public EntityTransaction getEntityTransaction() {
        return entityTransaction;
    }
}
