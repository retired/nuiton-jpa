package org.nuiton.jpa.api;

/*
 * #%L
 * Nuiton Jpa :: API
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * A AbstractJpaEntity must have a technical id.
 * <p/>
 * Also equals() is defined for compliance with JPA expectations.
 *
 * @author bleny <leny@codelutin.com>
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public abstract class AbstractJpaEntity implements JpaEntity {

    @Override
    public boolean equals(Object other) {
        String id = getId();
        if (other instanceof AbstractJpaEntity) {
            AbstractJpaEntity that = (AbstractJpaEntity) other;
            return id != null && that.getId() != null && id.equals(that.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
        String id = getId();
        return id == null ? 0 : id.hashCode();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{id=" + getId() + "}";
    }

}
