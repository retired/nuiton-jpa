package org.nuiton.jpa.hibernate;

/*
 * #%L
 * Nuiton Jpa :: Hibernate
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.cfg.NamingStrategy;

import java.util.List;

/**
 * Oracle doesn't support table names and columns names longer than 30 characters.
 *
 * This naming strategy solve the issue. <strong>This is not safe, since reducing
 * the name can lead to collisions according to your classes and their attributes names
 * </strong>
 *
 * To use it, add the following configuration in Hibernate:
 *
 * <pre>
 * hibernate.ejb.naming_strategy=org.nuiton.jpa.hibernate.OracleCompliantImprovedNamingStrategy
 * </pre>
 */
public class OracleCompliantImprovedNamingStrategy extends ImprovedNamingStrategy implements NamingStrategy {

    /**
     * Arbitrary value fixed by Oracle.
     *
     * @link http://stackoverflow.com/questions/8701479/table-name-with-more-than-30-characters
     */
    protected static final int MAXIMUM_IDENTIFIER_LENGTH = 30;

    protected BiMap<String, String> identifierToShortenedIdentifier = HashBiMap.create();

    /**
     * Given an identifier, return a similar identifier but no longer
     * than MAXIMUM_IDENTIFIER_LENGTH. Some characters are removed
     * at the end of each part of the identifier.
     *
     * Any given identifier shorter than MAXIMUM_IDENTIFIER_LENGTH
     * will be returned as it.
     *
     * @param value a string that may contain some underscores
     * @return a string that looks like given value but without
     *         removed characters in order to make the total
     *         length lower than MAXIMUM_IDENTIFIER_LENGTH
     */
    protected String getShortenedIdentifier(String value) {
        String reducedValue = value;
        if (value.length() > MAXIMUM_IDENTIFIER_LENGTH) {
            reducedValue = identifierToShortenedIdentifier.get(value);
            if (reducedValue == null) {
                reducedValue = shortenIdentifier(value);
                checkConflict(value, reducedValue);
                identifierToShortenedIdentifier.put(value, reducedValue);
            }
        }
        return reducedValue;
    }

    protected void checkConflict(String value, String reducedValue) {
        boolean conflictInShortenedIdentifier = identifierToShortenedIdentifier.inverse().containsKey(reducedValue);
        if (conflictInShortenedIdentifier) {
            String otherIdentifier = identifierToShortenedIdentifier.inverse().get(reducedValue);
            throw new HibernateException(
                    "error while using this naming strategy, a conflict occurred when short identifier for '"
                     + value + "' was computed. Generated short identifier (" + reducedValue +
                     ") is already used for identifier " + otherIdentifier);
        }
    }

    protected String shortenIdentifier(String identifier) {
        Iterable <String> split = Splitter.on('_').split(identifier);
        int numberOfElements = Iterables.size(split);
        int numberOfUnderscores = numberOfElements - 1;
        int acceptedLengthForAnElement = (MAXIMUM_IDENTIFIER_LENGTH - numberOfUnderscores) / numberOfElements;
        List<String> reducedElements = Lists.newLinkedList();
        for (String element : split) {
            reducedElements.add(StringUtils.left(element, acceptedLengthForAnElement));
        }
        String reducedIdentifier = Joiner.on('_').join(reducedElements);
        return reducedIdentifier;
    }

    @Override
    public String classToTableName(String className) {
        return getShortenedIdentifier(super.classToTableName(className));
    }

    @Override
    public String propertyToColumnName(String propertyName) {
        return getShortenedIdentifier(super.propertyToColumnName(propertyName));
    }

    @Override
    public String tableName(String tableName) {
        return getShortenedIdentifier(super.tableName(tableName));
    }

    @Override
    public String columnName(String columnName) {
        return getShortenedIdentifier(super.columnName(columnName));
    }

    @Override
    public String collectionTableName(String ownerEntity, String ownerEntityTable, String associatedEntity, String associatedEntityTable, String propertyName) {
        return getShortenedIdentifier(super.collectionTableName(ownerEntity, ownerEntityTable, associatedEntity, associatedEntityTable, propertyName));
    }

    @Override
    public String joinKeyColumnName(String joinedColumn, String joinedTable) {
        return getShortenedIdentifier(super.joinKeyColumnName(joinedColumn, joinedTable));
    }

    @Override
    public String foreignKeyColumnName(String propertyName, String propertyEntityName, String propertyTableName, String referencedColumnName) {
        return getShortenedIdentifier(super.foreignKeyColumnName(propertyName, propertyEntityName, propertyTableName, referencedColumnName));
    }

    @Override
    public String logicalColumnName(String columnName, String propertyName) {
        return getShortenedIdentifier(super.logicalColumnName(columnName, propertyName));
    }

    @Override
    public String logicalCollectionTableName(String tableName, String ownerEntityTable, String associatedEntityTable, String propertyName) {
        return getShortenedIdentifier(super.logicalCollectionTableName(tableName, ownerEntityTable, associatedEntityTable, propertyName));
    }

    @Override
    public String logicalCollectionColumnName(String columnName, String propertyName, String referencedColumn) {
        return getShortenedIdentifier(super.logicalCollectionColumnName(columnName, propertyName, referencedColumn));
    }

}
