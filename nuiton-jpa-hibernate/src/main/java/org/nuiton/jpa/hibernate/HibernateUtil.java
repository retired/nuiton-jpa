package org.nuiton.jpa.hibernate;

/*
 * #%L
 * Nuiton Jpa :: Hibernate
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.ejb.AvailableSettings;
import org.hibernate.ejb.EntityManagerFactoryImpl;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.internal.SessionFactoryServiceRegistryImpl;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.File;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class HibernateUtil {

    private static final Log log = LogFactory.getLog(HibernateUtil.class);

    protected static final Map<String, String> HIBERNATE_H2_CONFIG;

    public static final String HBM2DDL_AUTO_VALIDATE = "validate";

    public static final String HBM2DDL_AUTO_CREATE_DROP = "create-drop";

    public static final String HBM2DDL_AUTO_CREATE = "create";

    public static final String HBM2DDL_AUTO_UPDATE = "update";

    static {

        Map<String, String> hibernateH2Config = new HashMap<String, String>();

        hibernateH2Config.put(AvailableSettings.JDBC_DRIVER, org.h2.Driver.class.getName());
        hibernateH2Config.put(AvailableSettings.JDBC_USER, "sa");
        hibernateH2Config.put(AvailableSettings.JDBC_PASSWORD, "");
        hibernateH2Config.put(Environment.DIALECT, org.hibernate.dialect.H2Dialect.class.getName());
        hibernateH2Config.put(Environment.HBM2DDL_AUTO, HBM2DDL_AUTO_UPDATE);

        HIBERNATE_H2_CONFIG = Collections.unmodifiableMap(hibernateH2Config);

    }

    private HibernateUtil() {
    }

    public static EntityManagerFactory createTempEntityManagerFactory(String persistenceUnitName, String context) {

        EntityManagerFactory tempEntityManagerFactory =
                createTempEntityManagerFactory(
                        persistenceUnitName,
                        context, Collections.<String, String>emptyMap());

        return tempEntityManagerFactory;

    }

    public static EntityManagerFactory createTempEntityManagerFactory(String persistenceUnitName, String context, Map<String, String> jpaParameters) {

        Map<String, String> allJpaParameters = new HashMap<String, String>();

        allJpaParameters.putAll(jpaParameters);

        allJpaParameters.putAll(HIBERNATE_H2_CONFIG);

        File tempDirFile = SystemUtils.getJavaIoTmpDir();

        File databaseFile = new File(tempDirFile, context);

        String h2dataPath = databaseFile.getAbsolutePath() + File.separator + "h2data";

        String jdbcUrl = "jdbc:h2:file:" + h2dataPath;

        allJpaParameters.put(AvailableSettings.JDBC_URL, jdbcUrl);

        if (log.isTraceEnabled()) {
            log.trace("will store H2 data in " + h2dataPath);
            log.trace("allJpaParameters = " + allJpaParameters);
        }

        if (log.isDebugEnabled()) {
            log.debug("jdbc url is\n" + jdbcUrl);
        }

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceUnitName, allJpaParameters);

        return entityManagerFactory;

    }

    public static void cleanDatabase(EntityManager entityManager) {

        if (log.isInfoEnabled()) {
            log.info("will clean database");
        }

        ServiceRegistry serviceRegistry =
                ((EntityManagerFactoryImpl) entityManager.getEntityManagerFactory())
                        .getSessionFactory().getServiceRegistry();

        Configuration configuration = null;
        try {
            Field configurationField = SessionFactoryServiceRegistryImpl.class.getDeclaredField("configuration");
            configurationField.setAccessible(true);
            configuration = (Configuration) configurationField.get(serviceRegistry);
        } catch (IllegalAccessException e) {
            if (log.isErrorEnabled()) {
                log.error("should not occur", e);
            }
        } catch (NoSuchFieldException e) {
            if (log.isErrorEnabled()) {
                log.error("should not occur", e);
            }
        }

        SchemaExport schemaExport = new SchemaExport(serviceRegistry, configuration);

        schemaExport.setHaltOnError(true);

        // drop
        schemaExport.execute(true, true, true, false);

        // create
        schemaExport.execute(true, true, false, true);

    }

}
