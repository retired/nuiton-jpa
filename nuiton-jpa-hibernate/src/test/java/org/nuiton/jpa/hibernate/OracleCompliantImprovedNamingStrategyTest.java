package org.nuiton.jpa.hibernate;

/*
 * #%L
 * Nuiton Jpa :: Hibernate
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.junit.Assert;
import org.junit.Test;

public class OracleCompliantImprovedNamingStrategyTest {

    protected OracleCompliantImprovedNamingStrategy oracleCompliantImprovedNamingStrategy = new OracleCompliantImprovedNamingStrategy();

    @Test
    public void testNamingStrategy() {

        String className = getLongValue("LONG_CLASS_NAME_");

        String ownerEntity = getLongValue("LONG_OWNER_ENTITY_");

        String ownerEntityTable = getLongValue("LONG_OWNER_ENTITY_TABLE_");

        String associatedEntity = getLongValue("LONG_ASSOCIATED_ENTITY_");

        String associatedEntityTable = getLongValue("LONG_ASSOCIATED_ENTITY_TABLE_");

        String propertyName = getLongValue("LONG_PROPERTY_NAME_");

        String tableName = getLongValue("LONG_TABLE_NAME_");

        String columnName = getLongValue("LONG_COLUMN_NAME_");

        String propertyEntityName = getLongValue("LONG_PROPERTY_ENTITY_NAME");

        String propertyTableName = getLongValue("LONG_PROPERTY_TABLE_NAME");

        String referencedColumnName = getLongValue("LONG_REFERENCED_COLUMN_NAME");

        String referencedColumn = getLongValue("LONG_REFERENCED_COLUMN");

        String joinedColumn = getLongValue("LONG_JOINED_COLUMN");

        String joinedTable = getLongValue("LONG_JOINED_TABLE");

        check(oracleCompliantImprovedNamingStrategy.classToTableName(className));

        check(oracleCompliantImprovedNamingStrategy.classToTableName("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));

        check(oracleCompliantImprovedNamingStrategy.collectionTableName(ownerEntity, ownerEntityTable, associatedEntity, associatedEntityTable, propertyName));

        check(oracleCompliantImprovedNamingStrategy.logicalCollectionTableName(tableName, ownerEntityTable, associatedEntityTable, propertyName));

        check(oracleCompliantImprovedNamingStrategy.columnName(columnName));

        check(oracleCompliantImprovedNamingStrategy.tableName(tableName));

        check(oracleCompliantImprovedNamingStrategy.foreignKeyColumnName(propertyName, propertyEntityName, propertyTableName, referencedColumnName));

        check(oracleCompliantImprovedNamingStrategy.propertyToColumnName(propertyName));

        check(oracleCompliantImprovedNamingStrategy.logicalCollectionColumnName(columnName, propertyName, referencedColumn));

        check(oracleCompliantImprovedNamingStrategy.logicalColumnName(columnName, propertyName));

        check(oracleCompliantImprovedNamingStrategy.joinKeyColumnName(joinedColumn, joinedTable));

    }

    protected void check(String reducedValue) {

        Assert.assertFalse(
                "'" + reducedValue + "' length (" + reducedValue.length() + ") is too long",
                reducedValue.length() > oracleCompliantImprovedNamingStrategy.MAXIMUM_IDENTIFIER_LENGTH);

        Assert.assertFalse(reducedValue.startsWith("_"));

        Assert.assertFalse(reducedValue.endsWith("_"));

    }

    protected String getLongValue(String value) {
        return StringUtils.rightPad(value, 50, "LONG");
    }

    @Test
    public void testConflictsRaiseException() {

        String longClassName1 = getLongValue("LONG_CLASS_NAME_") + "1";
        String longClassName2 = getLongValue("LONG_CLASS_NAME_") + "2";

        // to lead to a conflict, we must check that both long class names
        // will have the same reduced identifier
        String shortenedIdentifier = oracleCompliantImprovedNamingStrategy.shortenIdentifier(longClassName1);
        Preconditions.checkState(
                shortenedIdentifier.equals(oracleCompliantImprovedNamingStrategy.shortenIdentifier(longClassName2))
        );

        // now check that an exception is raised and exception message is explicit
        oracleCompliantImprovedNamingStrategy.classToTableName(longClassName1);

        try {
            oracleCompliantImprovedNamingStrategy.classToTableName(longClassName2);
            Assert.fail("an exception should have been raised");
        } catch (HibernateException e) {
            Assert.assertTrue(StringUtils.containsIgnoreCase(e.getMessage(), longClassName1));
            Assert.assertTrue(StringUtils.containsIgnoreCase(e.getMessage(), longClassName2));
            Assert.assertTrue(StringUtils.containsIgnoreCase(e.getMessage(), shortenedIdentifier));
        }

    }
}
