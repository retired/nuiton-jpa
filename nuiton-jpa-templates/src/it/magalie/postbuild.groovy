/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

outputDir = new File(basedir, 'target/generated-sources/java');

file = new File(outputDir, 'com/franciaflex/magalie/persistence');
assert file.exists();
assert file.isDirectory();

file = new File(outputDir, 'com/franciaflex/magalie/persistence/entity');
assert file.exists();
assert file.isDirectory();

file = new File(outputDir, 'com/franciaflex/magalie/persistence/dao');
assert file.exists();
assert file.isDirectory();

return true;
