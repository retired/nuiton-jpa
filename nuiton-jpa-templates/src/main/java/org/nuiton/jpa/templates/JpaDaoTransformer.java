package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.jpa.api.AbstractJpaDao;

import java.util.Collection;
import java.util.List;

/**
 * JpaDaoTransformer generates a dao for an entity.
 * <p/>
 * <ul>
 * <li>{@code AbstractXXXJpaDao}: abstract jpa dao for entity named {@code XXX}, will find here the generated stuff (with jpa mapping, technical stuff)</li>
 * <li>{@code XXXJpaDao}: concrete public jpa dao to use in your persistence layer</li>
 * </ul>
 * {@code Note:} All classes found in class-path are not generated.
 *
 * @author tchemit <chemit@codelutin.com>
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.jpa.templates.JpaDaoTransformer"
 * @since 1.0
 */
public class JpaDaoTransformer extends AbstractJpaTransformer {

    private static final Log log = LogFactory.getLog(JpaDaoTransformer.class);


    @Override
    public void transformFromClass(ObjectModelClass input) {

        String packageName = JpaTemplatesGeneratorUtil.getDaoPackage(this, model, input);

        String abstractDaoName = JpaTemplatesGeneratorUtil.getDaoAbstractName(input);
        String concreteDaoName = JpaTemplatesGeneratorUtil.getDaoConcreteName(input);

        String concreteEntityQualifiedName =
                JpaTemplatesGeneratorUtil.getConcreteEntityQualifiedName(this, model, input);

        boolean generateAbstract = !isInClassPath(packageName, abstractDaoName);

        boolean generateConcrete = !isInClassPath(packageName, concreteDaoName);

        if (generateAbstract) {

            generateAbstract(input, packageName, abstractDaoName, concreteEntityQualifiedName);
        }

        if (generateConcrete) {

            generateImpl(input, packageName, abstractDaoName, concreteDaoName, concreteEntityQualifiedName);
        }
    }

    protected ObjectModelClass generateAbstract(ObjectModelClass input,
                                                String packageName,
                                                String abstractDaoName,
                                                String concreteEntityQualifiedName) {

        String prefix = getConstantPrefix(input, DEFAULT_CONSTANT_PREFIX);
        setConstantPrefix(prefix);

        String entityName = input.getName();

        ObjectModelClass output =
                createAbstractClass(abstractDaoName, packageName);
        addImport(output, concreteEntityQualifiedName);

        // test if a super class is in same package (so is yet another entity)
        boolean superClassIsEntity = isSuperClassEntity(input);

        String superClass;

        if (superClassIsEntity) {

            // get first super-class
            ObjectModelClass superClassModel = input.getSuperclasses().iterator().next();
            superClass = JpaTemplatesGeneratorUtil.getDaoPackage(this, model, superClassModel) +
                         "." +
                         JpaTemplatesGeneratorUtil.getDaoConcreteName(superClassModel);

        } else {

            // try to find a super class by tag-value
            superClass = JpaTemplatesGeneratorUtil.getDaoSuperClassTagValue(
                    model, input);

            if (superClass == null) {

                // no super-class, use default one
                superClass = AbstractJpaDao.class.getName() + "<" + entityName + ">";
                addImport(output, superClass);
            }
        }
        setSuperClass(output, superClass);

        addConstructorWithEntityManager(output);

        // detect if there is a contract to set on abstract
        String daoContractName = packageName + "." + entityName + "Dao";

        boolean addUserDaoContract = isInClassPath(daoContractName);

        if (addUserDaoContract) {
            addInterface(output, daoContractName);
        }

        // Add getEntityClass

        ObjectModelOperation operation = addOperation(output, "getEntityClass", "Class<" + entityName + ">", ObjectModelJavaModifier.PROTECTED);
        addAnnotation(output, operation, Override.class);
        setOperationBody(operation, ""
    /*{
        return <%=entityName%>.class;
    }*/
        );

        Collection<ObjectModelAttribute> attributes = input.getAttributes();
        if (CollectionUtils.isNotEmpty(attributes)) {
            addImport(output, List.class);
        }

        for (ObjectModelAttribute attr : attributes) {
            if (!attr.isNavigable()) {
                continue;
            }

            if (!GeneratorUtil.isNMultiplicity(attr)) {
                generateNoNMultiplicity(entityName, output, attr, false);
            } else {
                generateNMultiplicity(entityName, output, attr);
            }
        }

        if (isVerbose()) {
            log.info("will generate " + output.getQualifiedName());
        }
        return output;
    }

    protected ObjectModelClass generateImpl(ObjectModelClass input,
                                            String packageName,
                                            String abstractDaoName,
                                            String concreteDaoName,
                                            String concreteEntityQualifiedName) {

        ObjectModelClass output = createClass(concreteDaoName, packageName);

        setSuperClass(output, packageName + '.' + abstractDaoName);
        addConstructorWithEntityManager(output);

        if (isVerbose()) {
            log.info("will generate " + output.getQualifiedName());
        }
        return output;
    }

    protected void generateNoNMultiplicity(String clazzName,
                                           ObjectModelClass result,
                                           ObjectModelAttribute attr,
                                           boolean isAssoc) {
        String attrName = attr.getName();
        String attrType = attr.getType();
        String propertyName = clazzName + "." + getConstantName(attrName);
        if (!isAssoc && attr.hasAssociationClass()) {
            String assocClassName = attr.getAssociationClass().getName();
            String assocAttrName = JpaTemplatesGeneratorUtil.getAssocAttrName(attr);
            // It is about transitivity : use the property to access the
            // associationClass + '.' + the property to access the expected
            // attribute
            // <class>.<attrAssoc> + '.' + <assocClass>.<attr>
            propertyName =
                    clazzName + '.' + getConstantName(assocAttrName) +
                    " + '.' + " +
                    assocClassName + '.' + getConstantName(attrName);
        }

        ObjectModelOperation op;
        op = addOperation(result,
                          getJavaBeanMethodName("findBy", attrName),
                          clazzName,
                          ObjectModelJavaModifier.PUBLIC);
        addParameter(op, attrType, "v");
        setOperationBody(op, ""
/*{
        <%=clazzName%> result = findByProperty(<%=propertyName%>, v);
        return result;
    }*/
        );

        op = addOperation(result,
                          getJavaBeanMethodName("findAllBy", attrName),
                          "List<" + clazzName + ">",
                          ObjectModelJavaModifier.PUBLIC);
        addParameter(op, attrType, "v");
        setOperationBody(op, ""
/*{
        List<<%=clazzName%>> result = findAllByProperty(<%=propertyName%>, v);
        return result;
    }*/
        );

        if (!isAssoc && attr.hasAssociationClass()) {
            String assocClassName = attr.getAssociationClass().getName();
            String assocClassFQN = attr.getAssociationClass().getQualifiedName();
            String assocAttrName = GeneratorUtil.getAssocAttrName(attr);
            String assocPropertyConstantName = getConstantName(assocAttrName);
            op = addOperation(result,
                              getJavaBeanMethodName("findBy", assocClassName),
                              clazzName,
                              ObjectModelJavaModifier.PUBLIC);
            addParameter(op, assocClassFQN, "value");
            setOperationBody(op, ""
/*{
        <%=clazzName%> result = findByProperty(<%=clazzName + "." + assocPropertyConstantName%>, value);
        return result;
    }*/
            );

            op = addOperation(result,
                              getJavaBeanMethodName("findAllBy", assocClassName),
                              "List<" + clazzName + ">",
                              ObjectModelJavaModifier.PUBLIC);
            addParameter(op, assocClassFQN, "value");
            setOperationBody(op, ""
/*{
        List<<%=clazzName%>> result = findAllByProperty(<%=clazzName + "." + assocPropertyConstantName%>, value);
        return result;
    }*/
            );
        }
    }

    protected void generateNMultiplicity(String clazzName,
                                         ObjectModelClass result,
                                         ObjectModelAttribute attr) {
        String attrName = attr.getName();
        String attrType = attr.getType();
        if (attr.hasAssociationClass()) {
            // do nothing for association class, too complex...
            return;
        }
        ObjectModelOperation op;
        // Since 2.4 do nothing, findContains and findAllContains are not generated anymore
        op = addOperation(result,
                          getJavaBeanMethodName("findContains", attrName),
                          clazzName,
                          ObjectModelJavaModifier.PUBLIC);
        addParameter(op, attrType, "v");
        setOperationBody(op, ""
/*{
        <%=clazzName%> result = findContains(<%=clazzName + "." + getConstantName(attrName)%>, v);
        return result;
    }*/
        );

        op = addOperation(result,
                          getJavaBeanMethodName("findAllContains", attrName),
                          "List<" + clazzName + ">",
                          ObjectModelJavaModifier.PUBLIC);
        addParameter(op, attrType, "v");
        setOperationBody(op, ""
/*{
        List<<%=clazzName%>> result = findAllContains(<%=clazzName + "." + getConstantName(attrName)%>, v);
        return result;
    }*/
        );
    }

}
