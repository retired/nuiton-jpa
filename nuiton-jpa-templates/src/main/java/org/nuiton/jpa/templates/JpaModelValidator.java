package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.validator.ObjectModelValidator;

/**
 * TODO
 *
 * @author tchemit <chemit@codelutin.com>
 * @since TODO
 */
public class JpaModelValidator extends ObjectModelValidator {
    public JpaModelValidator(ObjectModel model) {
        super(model);
    }

    boolean useIdGenerator;

    @Override
    protected boolean validateModel(ObjectModel model) {

        boolean useIdGenerator = JpaTemplatesGeneratorUtil.useIdFactory(model);
        return super.validateModel(model);
    }

    @Override
    protected boolean validateClassifier(ObjectModelClassifier clazz) {
        if (JpaTemplatesGeneratorUtil.isEntity(clazz)) {

            if (useIdGenerator) {

                // can't have any stuff around GeneraturValue
                if (JpaTemplatesGeneratorUtil.hasNotGeneratedValueStereotype(clazz)) {

                    addError(clazz, "Can't use a NotGenerated tag value when using an id factory");
                }
            }
        }
        return super.validateClassifier(clazz);
    }
}
