package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.java.extension.ObjectModelAnnotation;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;
import org.nuiton.jpa.api.AbstractJpaEntity;
import org.nuiton.jpa.api.JpaEntities;
import org.nuiton.jpa.api.JpaEntity;
import org.nuiton.jpa.api.JpaEntityIdFactoryResolver;
import org.nuiton.jpa.api.JpaEntityVisitable;
import org.nuiton.jpa.api.JpaEntityVisitor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.PrePersist;
import javax.persistence.Version;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * JpaEntityTransformer generates two type of classes:
 * <ul>
 * <li>{@code AbstractJpaXXXEntity}: abstract jpa entity for entity named {@code XXX}, will find here the generated stuff (with jpa mapping, technical stuff) </li>
 * <li>{@code JpaXXXEntity}: concrete public jpa entity to use in your persistence layer</li>
 * </ul>
 * <p/>
 * {@code Note:} All classes found in class-path are not generated.
 *
 * @author tchemit <chemit@codelutin.com>
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.jpa.templates.JpaEntityTransformer"
 * @since 1.0
 */
public class JpaEntityTransformer extends AbstractJpaTransformer {

    private static final Log log =
            LogFactory.getLog(JpaEntityTransformer.class);

    public static final String PROPERTY_VERSION = "version";

    public static final String PROPERTY_CREATE_DATE = "createDate";

    @Override
    public void transformFromClass(ObjectModelClass input) {

        String packageName = JpaTemplatesGeneratorUtil.getEntityPackage(this, model, input);

        String entityAbstractName = JpaTemplatesGeneratorUtil.getEntityAbstractName(input);

        String entityConcreteName = JpaTemplatesGeneratorUtil.getEntityConcreteName(input);

        boolean generateAbstract = !isInClassPath(packageName,
                                                  entityAbstractName);

        boolean generateConcrete = !isInClassPath(packageName,
                                                  entityConcreteName);

        if (generateAbstract) {

            generateAbstract(input,
                             packageName,
                             entityAbstractName);
        }

        if (generateConcrete) {

            generateImpl(input,
                         packageName,
                         entityAbstractName,
                         entityConcreteName);
        }
    }

    protected ObjectModelClass generateAbstract(ObjectModelClass input,
                                                String packageName,
                                                String entityAbstractName) {

        // test if a super class is in same package (so is yet another entity)
        boolean superClassIsEntity = isSuperClassEntity(input);

        String superClass;

        if (superClassIsEntity) {

            // get first super-class
            ObjectModelClass superClassModel =
                    input.getSuperclasses().iterator().next();
            superClass = JpaTemplatesGeneratorUtil.getEntityPackage(this, model, superClassModel) +
                         "." +
                         JpaTemplatesGeneratorUtil.getEntityConcreteName(superClassModel);
        } else {

            // try to find a super class by tag-value
            superClass = JpaTemplatesGeneratorUtil.getEntitySuperClassTagValue(
                    model, input);

            if (superClass == null) {

                // no super-class, use default one
                superClass = AbstractJpaEntity.class.getName();

            } else {

                // will act as if super class is a bean
                superClassIsEntity = true;
            }
        }

        ObjectModelClass output =
                createAbstractClass(entityAbstractName, packageName);

        setSuperClass(output, superClass);

        addAnnotation(output, output, MappedSuperclass.class);

        // detect if there is a contract to set on abstract
        String entityContractName = packageName + "." + input.getName() + "Entity";

        boolean addUserEntityContract = isInClassPath(entityContractName);

        if (addUserEntityContract) {
            addInterface(output, entityContractName);
        }

        String prefix = getConstantPrefix(input, DEFAULT_CONSTANT_PREFIX);

        setConstantPrefix(prefix);

        boolean serializableFound = addInterfaces(input, output);

        if (superClassIsEntity) {
            serializableFound = true;
        }

        addSerializable(input, output, serializableFound);

        boolean useIdGenerator =
                JpaTemplatesGeneratorUtil.useIdFactory(getModel());

        if (useIdGenerator) {

            // use an idGenerator

            // add prePersist method
            ObjectModelOperation operation = addOperation(
                    output,
                    "prePersist",
                    void.class,
                    ObjectModelJavaModifier.PUBLIC);
            addAnnotation(output, operation, PrePersist.class);
            addImport(output, JpaEntityIdFactoryResolver.class);
            setOperationBody(operation, ""
                             /*{
        if (this.id == null) {
            this.id = new JpaEntityIdFactoryResolver().newId(this);
        }
    }*/
            );
        }

        boolean generateVisitor =
                JpaTemplatesGeneratorUtil.useGenerateVisitors(getModel());

        if (generateVisitor) {
            addInterface(output, JpaEntityVisitable.class);

            // add accept method
            ObjectModelOperation operation = addOperation(
                    output,
                    "accept",
                    void.class,
                    ObjectModelJavaModifier.PUBLIC);
            addAnnotation(output, operation, Override.class);
            addParameter(operation, JpaEntityVisitor.class, "visitor");
            setOperationBody(operation, ""
    /*{
        visitor.visit(this);
    }*/
            );
        }

        boolean usePCS =
                JpaTemplatesGeneratorUtil.useGeneratePropertyChangeListeners(
                        getModel());

        Set<String> constantNames = addConstantsFromDependency(input, output);

        // Get available properties
        List<ObjectModelAttribute> properties =
                JpaTemplatesGeneratorUtil.getProperties(input);

        // Create an id property
        ObjectModelAttributeImpl idAttr = new ObjectModelAttributeImpl();
        idAttr.setDeclaringElement(input);
        idAttr.setName(JpaEntity.PROPERTY_ID);
        idAttr.setType(String.class.getName());
        properties.add(0, idAttr);

        boolean generateTechnicalFields =
                JpaTemplatesGeneratorUtil.useGenerateExtraTechnicalFields(getModel(), input);

        if (generateTechnicalFields) {

            // add version field
            ObjectModelAttributeImpl versionAttr = new ObjectModelAttributeImpl();
            versionAttr.setDeclaringElement(input);
            versionAttr.setName(PROPERTY_VERSION);
            versionAttr.setType(int.class.getName());
            properties.add(1, versionAttr);

            // add createDate field
            ObjectModelAttributeImpl createDateAttr = new ObjectModelAttributeImpl();
            createDateAttr.setDeclaringElement(input);
            createDateAttr.setName(PROPERTY_CREATE_DATE);
            createDateAttr.setType(Date.class.getName());
            createDateAttr.setDefaultValue("new Date()");
            properties.add(2, createDateAttr);
        }


        //TODO Si ORDERED ou et UNIQUE choisir le bon type de collection (O+U = LHS (laisser ça comme definition), (U = HS + S), (O = ArrayList +L), ( = ArrayList + C)
        //TODO Si enumeration @Enumeration(value = EnumType.STRING) (Voir pour mettre une tag-value pour mettre en ordinal) + voir comment ça marche pour les collections

//        Multimap<ObjectModelAttribute, String> annotationsForAttributes =
//                generateAnnotationsForProperties(input,
//                                                 output,
//                                                 properties,
//                                                 generateTechnicalFields);

        // Add properties constant
        for (ObjectModelAttribute attr : properties) {

            createPropertyConstant(output, attr, prefix, constantNames);
        }

        // Add properties field + javabean methods
        for (ObjectModelAttribute attr : properties) {

//            createProperty(output, attr, usePCS, annotationsForAttributes.get(attr));
            createProperty(input, output, attr, usePCS, generateTechnicalFields);
        }

        if (usePCS) {

            // Add property change support
            createPropertyChangeSupport(output);
        }

        boolean hasAMultipleProperty =
                JpaTemplatesGeneratorUtil.containsMutiple(properties);

        // Add helper operations
        if (hasAMultipleProperty) {

            // add getChild methods
            createGetChildMethod(output);

            addImport(output, JpaEntities.class);
        }

        if (isVerbose()) {
            log.info("will generate " + output.getQualifiedName());
        }
        return output;
    }

    protected ObjectModelClass generateImpl(ObjectModelClass input,
                                            String packageName,
                                            String entityAbstractName,
                                            String entityConcreteName) {

        ObjectModelClass output = createClass(entityConcreteName, packageName);

        setSuperClass(output, entityAbstractName);

        // add Entity annotation
        addAnnotation(output, output, Entity.class);

        // add a fix serialVersionUID, since the class has no field nor method
        addConstant(output,
                    JpaTemplatesGeneratorUtil.SERIAL_VERSION_UID,
                    "long",
                    "1L",
                    ObjectModelJavaModifier.PRIVATE
        );

        if (isVerbose()) {
            log.info("will generate " + output.getQualifiedName());
        }
        return output;
    }

    protected void addPropertyAnnotations(ObjectModelClass input,
                                          ObjectModelClass output,
                                          ObjectModelAttribute inputProperty,
                                          ObjectModelAttribute property,
                                          boolean generateTechnicalFields) {
            String propertyName = inputProperty.getName();
            String propertyType = inputProperty.getType();
            if (JpaEntity.PROPERTY_ID.equals(propertyName)) {

//                addAnnotation(result, property, output, Id.class);
                addAnnotation(output, property, Id.class);
                if (JpaTemplatesGeneratorUtil.generateEntityGeneratedValueAnnotation(model, input)) {
//                    addAnnotation(result, property, output, GeneratedValue.class);
                    addAnnotation(output, property, GeneratedValue.class);
                }
                return;
            }

            if (PROPERTY_VERSION.equals(propertyName) && generateTechnicalFields) {

                addAnnotation(output, property, Version.class);
                return;
            }

            ObjectModelAttribute reverseAttribute =
                    inputProperty.getReverseAttribute();

            boolean nMultiplicity = reverseAttribute != null &&
                                    JpaTemplatesGeneratorUtil.isNMultiplicity(reverseAttribute);

            boolean bidirection = reverseAttribute != null &&
                                  reverseAttribute.isNavigable();

            boolean isInverse = false;

            if (bidirection) {

                // compute which is master of relation
                String inverseValue = JpaTemplatesGeneratorUtil.getInverseTagValue(inputProperty);
                if (StringUtils.isNotEmpty(inverseValue)) {
                    isInverse = Boolean.parseBoolean(inverseValue);
                    // Si aucun tagvalue n'est défini, le choix est arbitraire : le
                    // premier attribut dans l'ordre alphabétique sera choisi pour porter le
                    // inverse="true"
                } else {
                    isInverse = JpaTemplatesGeneratorUtil.isFirstAttribute(inputProperty);
                }
            }

            boolean composite = inputProperty.isComposite();

            boolean entity = isEntity(input, propertyType);
            boolean enumeration = getModel().getEnumeration(propertyType) != null;

            if (JpaTemplatesGeneratorUtil.isNMultiplicity(inputProperty)) {

//                List<String> annotationParams = Lists.newArrayList();
//
//                if (composite) {
//                    annotationParams.add("cascade = CascadeType.ALL");
//                    annotationParams.add("orphanRemoval = true");
//                    addImport(output, CascadeType.class);
//                }
//
//                if (bidirection && !isInverse) {
//
//                    // add the mappedBy parameter
//                    annotationParams.add("mappedBy = \"" + reverseAttribute.getName() + "\"");
//                }

                ObjectModelAnnotation annotation;

                if (nMultiplicity) {

                    // * -> * (ManyToMany)
                    annotation = addAnnotation(output, property, ManyToMany.class);

                } else {

                    // 0..1 -> * (OneToMany)
                    annotation = addAnnotation(output, property, OneToMany.class);
                }

                if (composite) {
                    addAnnotationParameter(output, annotation, "cascade", CascadeType.ALL);
                    addAnnotationParameter(output, annotation, "orphanRemoval", true);
                }

                if (bidirection && !isInverse) {

                    // add the mappedBy parameter
                    addAnnotationParameter(output, annotation, "mappedBy", reverseAttribute.getName());
                }


                boolean isOrdered = JpaTemplatesGeneratorUtil.isOrdered(inputProperty);

                if (isOrdered) {
                    addAnnotation(output, property, OrderColumn.class);
                }
                return;
            }

            if (enumeration) {

                // is an enumeration
                ObjectModelAnnotation annotation =
                        addAnnotation(output, property, Enumerated.class);
                addAnnotationParameter(output, annotation, "value", EnumType.STRING);
            }

            if (entity) {

                if (nMultiplicity) {

                    // * -> 0..1 (ManyToOne)
                    addAnnotation(output, property, ManyToOne.class);
                } else {

                    // 0..1 -> 0..1 (OneToOne)
                    addAnnotation(output, property, OneToOne.class);
                }
            }
    }

    protected void createProperty(ObjectModelClass input,
                                  ObjectModelClass output,
                                  ObjectModelAttribute attr,
                                  boolean usePCS,
                                  boolean geneTechnicalFields) {

        String attrName = JpaTemplatesGeneratorUtil.getAttributeName(attr);
        String attrType = JpaTemplatesGeneratorUtil.getAttributeType(attr);

        boolean multiple = JpaTemplatesGeneratorUtil.isNMultiplicity(attr);

        String constantName = getConstantName(attrName);
        String simpleType = JpaTemplatesGeneratorUtil.getSimpleName(attrType);

        if (multiple) {

            Class<?> collectionType =
                    JavaGeneratorUtil.getCollectionType(attr);

            Class<?> collectionInstanceType =
                    JavaGeneratorUtil.getCollectionInstanceType(attr);

            createGetChildMethod(output,
                                 attrName,
                                 attrType,
                                 simpleType);

            createGetChildByIdMethod(output,
                                     attrName,
                                     attrType,
                                     simpleType);

            createIsEmptyMethod(output,
                                attrName);

            createSizeMethod(output,
                             attrName);

            createAddChildMethod(output,
                                 attrName,
                                 attrType,
                                 constantName,
                                 collectionInstanceType,
                                 usePCS);

            createAddAllChildrenMethod(output,
                                       attrName,
                                       attrType,
                                       constantName,
                                       collectionInstanceType,
                                       usePCS);

            createRemoveChildMethod(output,
                                    attrName,
                                    attrType,
                                    constantName,
                                    usePCS);

            createRemoveAllChildrenMethod(output,
                                          attrName,
                                          attrType,
                                          constantName,
                                          usePCS);

            createContainsChildMethod(output,
                                      attrName,
                                      attrType,
                                      constantName,
                                      usePCS);

            createContainsChildByIdMethod(output,
                                          attrName,
                                          attrType,
                                          constantName,
                                          usePCS);

            createContainsAllChildrenMethod(output,
                                            attrName,
                                            attrType,
                                            constantName,
                                            usePCS);

            // Change type for Multiple attribute
            attrType = collectionType.getName() + "<" + attrType + ">";

            simpleType = JpaTemplatesGeneratorUtil.getSimpleName(attrType);
        }

        boolean booleanProperty = JpaTemplatesGeneratorUtil.isBooleanPrimitive(attr);

        String getterPrefix;
        if (booleanProperty && !multiple) {
            getterPrefix = JpaTemplatesGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX;
        } else {
            getterPrefix = JpaTemplatesGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;

        }
        if (booleanProperty && !multiple) {

            // creates a isXXX method
            createGetMethod(output,
                            attrName,
                            attrType,
                            getterPrefix
            );
        }

        if (multiple || !booleanProperty) {

            ObjectModelOperation getMethod = createGetMethod(output,
                                                             attrName,
                                                             attrType,
                                                             getterPrefix
            );

            if (JpaEntity.PROPERTY_ID.equals(attrName)) {
                addAnnotation(output, getMethod, Override.class);
            }

        }
        createSetMethod(output,
                        attrName,
                        attrType,
                        simpleType,
                        constantName,
                        getterPrefix,
                        usePCS
        );

        // Add attribute to the class
        ObjectModelAttribute attribute = addAttribute(output,
                                                      attrName,
                                                      attrType,
                                                      attr.getDefaultValue(),
                                                      ObjectModelJavaModifier.PROTECTED
        );

        addPropertyAnnotations(input,
                               output,
                               attr,
                               attribute,
                               geneTechnicalFields);
    }

    protected ObjectModelOperation createGetMethod(ObjectModelClass output,
                                                   String attrName,
                                                   String attrType,
                                                   String methodPrefix) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName(methodPrefix, attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%>;
    }*/
        );
        return operation;
    }

    protected void createGetChildMethod(ObjectModelClass output,
                                        String attrName,
                                        String attrType,
                                        String simpleType) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("get", attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "int", "index");
        setOperationBody(operation, ""
    /*{
        <%=simpleType%> o = getChild(<%=attrName%>, index);
        return o;
    }*/
        );
    }

    protected void createGetChildByIdMethod(ObjectModelClass output,
                                            String attrName,
                                            String attrType,
                                            String simpleType) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("get", attrName + "ById"),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "String", "id");
        setOperationBody(operation, ""
    /*{
        <%=simpleType%> o = JpaEntities.findById(<%=attrName%>, id);
        return o;
    }*/
        );
    }

    protected void createIsEmptyMethod(ObjectModelClass output,
                                       String attrName) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("is", attrName) + "Empty",
                boolean.class,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%> == null || <%=attrName%>.isEmpty();
    }*/
        );
    }

    protected void createSizeMethod(ObjectModelClass output,
                                    String attrName) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("size", attrName),
                int.class,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%> == null ? 0 : <%=attrName%>.size();
    }*/
        );
    }

    protected void createAddChildMethod(ObjectModelClass output,
                                        String attrName,
                                        String attrType,
                                        String constantName,
                                        Class<?> collectionInstanceType,
                                        boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("add", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        addImport(output, collectionInstanceType);
        String getMethodName = getJavaBeanMethodName("get", attrName);
        String setMethodName = getJavaBeanMethodName("set", attrName);
        String instanceName = collectionInstanceType.getSimpleName() + "<" + JpaTemplatesGeneratorUtil.getSimpleName(attrType) + ">";
        StringBuilder buffer = new StringBuilder(""
    /*{
        if (<%=getMethodName%>() == null) {
            <%=setMethodName%>(new <%=instanceName%>());
        }
        <%=getMethodName%>().add(<%=attrName%>);
    }*/
        );
        if (usePCS) {
            buffer.append(""
    /*{    firePropertyChange(<%=constantName%>, null, <%=attrName%>);
    }*/
            );
        }
        setOperationBody(operation, buffer.toString());
    }

    protected void createAddAllChildrenMethod(ObjectModelClass output,
                                              String attrName,
                                              String attrType,
                                              String constantName,
                                              Class<?> collectionInstanceType,
                                              boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("addAll", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, Collection.class.getName() + "<" + attrType + ">", attrName);

        String getMethodName = getJavaBeanMethodName("get", attrName);
        String setMethodName = getJavaBeanMethodName("set", attrName);
        String instanceName = collectionInstanceType.getSimpleName() + "<" + JpaTemplatesGeneratorUtil.getSimpleName(attrType) + ">";
        StringBuilder buffer = new StringBuilder(""
    /*{
        if (<%=getMethodName%>() == null) {
            <%=setMethodName%>(new <%=instanceName%>());
        }
        <%=getMethodName%>().addAll(<%=attrName%>);
    }*/
        );
        if (usePCS) {
            buffer.append(""
    /*{    firePropertyChange(<%=constantName%>, null, <%=attrName%>);
    }*/
            );
        }
        setOperationBody(operation, buffer.toString());
    }

    protected void createRemoveChildMethod(ObjectModelClass output,
                                           String attrName,
                                           String attrType,
                                           String constantName,
                                           boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("remove", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        String methodName = getJavaBeanMethodName("get", attrName);
        StringBuilder buffer = new StringBuilder();
        buffer.append(""
    /*{
        boolean removed = <%=methodName%>() != null && <%=methodName%>().remove(<%=attrName%>);}*/
        );

        if (usePCS) {
            buffer.append(""
    /*{
        if (removed) {
            firePropertyChange(<%=constantName%>, <%=attrName%>, null);
        }}*/
            );
        }
        buffer.append(""
    /*{
        return removed;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createRemoveAllChildrenMethod(ObjectModelClass output,
                                                 String attrName,
                                                 String attrType,
                                                 String constantName,
                                                 boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("removeAll", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "java.util.Collection<" + attrType + ">", attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean  removed = <%=methodName%>() != null && <%=methodName%>().removeAll(<%=attrName%>);}*/
        );

        if (usePCS) {
            buffer.append(""
    /*{
        if (removed) {
            firePropertyChange(<%=constantName%>, <%=attrName%>, null);
        }}*/
            );
        }
        buffer.append(""
    /*{
        return removed;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createContainsChildMethod(ObjectModelClass output,
                                             String attrName,
                                             String attrType,
                                             String constantName,
                                             boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("contains", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean contains = <%=methodName%>() != null && <%=methodName%>().contains(<%=attrName%>);
        return contains;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createContainsChildByIdMethod(ObjectModelClass output,
                                                 String attrName,
                                                 String attrType,
                                                 String constantName,
                                                 boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("contains", attrName + "ById"),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "String", "id");
        StringBuilder buffer = new StringBuilder();
        String getMethodName = getJavaBeanMethodName("get", attrName);
        String methodName = getJavaBeanMethodName("get", attrName + "ById");
        buffer.append(""
    /*{
        boolean contains = <%=getMethodName%>() != null && <%=methodName%>(id) != null;
        return contains;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createContainsAllChildrenMethod(ObjectModelClass output,
                                                   String attrName,
                                                   String attrType,
                                                   String constantName,
                                                   boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("containsAll", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, Collection.class.getName() + "<" + attrType + ">", attrName);
        StringBuilder buffer = new StringBuilder();
        String getMethodName = getJavaBeanMethodName("get", attrName);
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean  contains = <%=getMethodName%>() != null && <%=methodName%>().containsAll(<%=attrName%>);
        return contains;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createSetMethod(ObjectModelClass output,
                                   String attrName,
                                   String attrType,
                                   String simpleType,
                                   String constantName,
                                   String getterPrefix,
                                   boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("set", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);

        if (usePCS) {
            String methodName = getJavaBeanMethodName(getterPrefix, attrName);
            setOperationBody(operation, ""
    /*{
        <%=simpleType%> oldValue = <%=methodName%>();
        this.<%=attrName%> = <%=attrName%>;
        firePropertyChange(<%=constantName%>, oldValue, <%=attrName%>);
    }*/
            );
        } else {
            setOperationBody(operation, ""
    /*{
        this.<%=attrName%> = <%=attrName%>;
    }*/
            );
        }
    }

    protected void createGetChildMethod(ObjectModelClass output) {
        ObjectModelOperation getChild = addOperation(
                output,
                "getChild", "<T> T",
                ObjectModelJavaModifier.PROTECTED
        );
        addImport(output, List.class);

        addParameter(getChild, "java.util.Collection<T>", "childs");
        addParameter(getChild, "int", "index");
        setOperationBody(getChild, ""
    /*{
        T result = null;
        if (childs != null) {
            if (childs instanceof List) {
                if (index < childs.size()) {
                    result = ((List<T>) childs).get(index);
                }
            } else {
                int i = 0;
                for (T o : childs) {
                    if (index == i) {
                        result = o;
                        break;
                    }
                    i++;
                }
            }
        }
        return result;
    }*/
        );
    }

    protected void createPropertyChangeSupport(ObjectModelClass output) {

        addAttribute(output,
                     "pcs",
                     PropertyChangeSupport.class,
                     "new PropertyChangeSupport(this)",
                     ObjectModelJavaModifier.PROTECTED,
                     ObjectModelJavaModifier.FINAL,
                     ObjectModelJavaModifier.TRANSIENT
        );

        // Add PropertyListener

        ObjectModelOperation operation;

        operation = addOperation(output,
                                 "addPropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.addPropertyChangeListener(listener);
    }*/
        );

        operation = addOperation(output,
                                 "addPropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.addPropertyChangeListener(propertyName, listener);
    }*/
        );

        operation = addOperation(output,
                                 "removePropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.removePropertyChangeListener(listener);
    }*/
        );

        operation = addOperation(output,
                                 "removePropertyChangeListener",
                                 "void",
                                 ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.removePropertyChangeListener(propertyName, listener);
    }*/
        );

        operation = addOperation(output,
                                 "firePropertyChange",
                                 "void",
                                 ObjectModelJavaModifier.PROTECTED
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, Object.class, "oldValue");
        addParameter(operation, Object.class, "newValue");
        setOperationBody(operation, ""
    /*{
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }*/
        );

        operation = addOperation(output,
                                 "firePropertyChange",
                                 "void",
                                 ObjectModelJavaModifier.PROTECTED
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, Object.class, "newValue");
        setOperationBody(operation, ""
    /*{
        firePropertyChange(propertyName, null, newValue);
    }*/
        );
    }
}
