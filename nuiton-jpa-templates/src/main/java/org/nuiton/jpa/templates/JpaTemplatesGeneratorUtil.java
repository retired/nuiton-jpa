package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.eugene.java.JavaGeneratorUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Utility class for pure jpa templates.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 2.7
 */
public class JpaTemplatesGeneratorUtil extends JavaGeneratorUtil {

    /**
     * Tests if the given class is an entity (Need only to be in a entity
     * package).
     *
     * @param input the class to test
     * @return {@code true} if class is an entity.
     */
    public static boolean isEntity(ObjectModelClassifier input) {
        return input.getPackageName().endsWith(".entity");
    }

    public static String getPersistenceContextPackage(AbstractJpaTransformer transformer,
                                                      ObjectModel model) {

        String result = transformer.getDefaultPackageName();

        return result;
    }

    public static String getEntityPackage(AbstractJpaTransformer transformer,
                                          ObjectModel model,
                                          ObjectModelClassifier input) {

        String result = input.getPackageName();
        return result;
    }

    public static String getDaoPackage(AbstractJpaTransformer transformer,
                                       ObjectModel model,
                                       ObjectModelClassifier input) {

        Preconditions.checkState(isEntity(input), "Cant' find dao package name for a none entity " + input);
        int lastIndexOf = input.getPackageName().lastIndexOf(".entity");
        String result = input.getPackageName().substring(0, lastIndexOf) + ".dao";
        return result;
    }

    /**
     * Obtain the list of entities classes with the possibility to sort the
     * result.
     *
     * @param model the current model to scan
     * @param sort  flag to allow sort the result
     * @return the list of filtred classes by their stereotype
     */
    public static List<ObjectModelClass> getEntityClasses(ObjectModel model,
                                                          boolean sort) {
        List<ObjectModelClass> classes = new ArrayList<ObjectModelClass>();
        for (ObjectModelClass clazz : model.getClasses()) {
            if (isEntity(clazz)) {
                classes.add(clazz);
            }
        }
        if (sort && !classes.isEmpty()) {

            Collections.sort(classes, OBJECT_MODEL_CLASS_COMPARATOR);
        }
        return classes;
    }

    static public final Comparator<ObjectModelClass>
            OBJECT_MODEL_CLASS_COMPARATOR =
            new Comparator<ObjectModelClass>() {

                @Override
                public int compare(ObjectModelClass o1,
                                   ObjectModelClass o2) {
                    return o1.getQualifiedName().compareTo(
                            o2.getQualifiedName());
                }
            };

    public static String getPersistenceContextInterfaceName(ObjectModel model) {
        return model.getName() + "PersistenceContext";
    }

    public static String getPersistenceContextAbstractName(ObjectModel model) {
        return "AbstractJpa" + model.getName() + "PersistenceContext";
    }

    public static String getPersistenceContextConcreteName(ObjectModel model) {
        return "Jpa" + model.getName() + "PersistenceContext";
    }

    public static String getEntityAbstractName(ObjectModelClass input) {
        return "AbstractJpa" + input.getName();
    }

    public static String getEntityConcreteName(ObjectModelClass input) {
        return input.getName();
    }

    public static String getDaoAbstractName(ObjectModelClass input) {
        return "Abstract" + input.getName() + "JpaDao";
    }

    public static String getDaoGeneratedName(ObjectModelClass input) {
        return "Generated" + input.getName() + "JpaDao";
    }

    public static String getDaoConcreteName(ObjectModelClass input) {
        return input.getName() + "JpaDao";
    }

    public static String getConcreteEntityQualifiedName(AbstractJpaTransformer transformer, ObjectModel model, ObjectModelClass input) {
        return getEntityPackage(transformer, model, input) + "." + getEntityConcreteName(input);
    }

    public static String getConcreteDaoQualifiedName(AbstractJpaTransformer transformer, ObjectModel model, ObjectModelClass input) {
        return getDaoPackage(transformer, model, input) + "." + getDaoConcreteName(input);
    }

    public static boolean containsMutiple(Collection<ObjectModelAttribute> attributes) {

        boolean result = false;

        for (ObjectModelAttribute attr : attributes) {

            if (isNMultiplicity(attr)) {
                result = true;

                break;
            }

        }
        return result;
    }

    public static String getAttributeName(ObjectModelAttribute attr) {
        String attrName = attr.getName();
        if (attr.hasAssociationClass()) {
            String assocAttrName = getAssocAttrName(attr);
            attrName = toLowerCaseFirstLetter(assocAttrName);
        }
        return attrName;
    }

    public static String getAttributeType(ObjectModelAttribute attr) {
        String attrType = attr.getType();
        if (attr.hasAssociationClass()) {
            attrType = attr.getAssociationClass().getName();
        }
        return attrType;
    }

    public static List<ObjectModelAttribute> getProperties(ObjectModelClass input) {
        List<ObjectModelAttribute> attributes =
                (List<ObjectModelAttribute>) input.getAttributes();

        List<ObjectModelAttribute> attrs =
                new ArrayList<ObjectModelAttribute>();
        for (ObjectModelAttribute attr : attributes) {
            if (attr.isNavigable()) {

                // only keep navigable attributes
                attrs.add(attr);
            }
        }
        return attrs;
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_ID_FACTORY}
     * tag value on the given model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null}
     *         if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_ID_FACTORY
     * @since 1.0
     */
    public static String getIdFactoryTagValue(ObjectModel model) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_ID_FACTORY, null, model);
        return value;
    }

    public static boolean useIdFactory(ObjectModel model) {
        String value = getIdFactoryTagValue(model);
        return ObjectUtils.equals("true", value);
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_GENERATE_PROPERTY_CHANGE_LISTENERS}
     * tag value on the given model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null}
     *         if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_GENERATE_PROPERTY_CHANGE_LISTENERS
     * @since 1.0
     */
    public static String getGeneratePropertyChangeListenersTagValue(ObjectModel model) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_GENERATE_PROPERTY_CHANGE_LISTENERS, null, model);
        return value;
    }

    public static boolean useGeneratePropertyChangeListeners(ObjectModel model) {
        String value = getGeneratePropertyChangeListenersTagValue(model);
        return ObjectUtils.equals("true", value);
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_GENERATE_VISITORS}
     * tag value on the given model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null}
     *         if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_GENERATE_VISITORS
     * @since 1.0
     */
    public static String getGenerateVisitorsTagValue(ObjectModel model) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_GENERATE_VISITORS, null, model);
        return value;
    }

    public static boolean useGenerateVisitors(ObjectModel model) {
        String value = getGenerateVisitorsTagValue(model);
        return ObjectUtils.equals("true", value);
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_GENERATE_EXTRA_TECHNICAL_FIELDS}
     * tag value on the given model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null}
     *         if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_GENERATE_EXTRA_TECHNICAL_FIELDS
     * @since 1.0
     */
    public static String getGenerateExtraTechnicalFieldsTagValue(ObjectModel model, ObjectModelClassifier classifier) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_GENERATE_EXTRA_TECHNICAL_FIELDS, null, model);
        return value;
    }

    public static boolean useGenerateExtraTechnicalFields(ObjectModel model,ObjectModelClassifier classifier) {
        String value = getGenerateExtraTechnicalFieldsTagValue(model, classifier);
        return ObjectUtils.equals("true", value);
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_ENTITY_SUPER_CLASS}
     * tag value on the given model or classifier.
     * <p/>
     * It will first look on the model, and then in the given classifier.
     *
     * @param model      model to seek
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_ENTITY_SUPER_CLASS
     * @since 1.0
     */
    public static String getEntitySuperClassTagValue(ObjectModel model, ObjectModelClassifier classifier) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_ENTITY_SUPER_CLASS, classifier, model);
        return value;
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_DAO_SUPER_CLASS}
     * tag value on the given model or classifier.
     * <p/>
     * It will first look on the model, and then in the given classifier.
     *
     * @param model      model to seek
     * @param classifier classifier to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_DAO_SUPER_CLASS
     * @since 1.0
     */
    public static String getDaoSuperClassTagValue(ObjectModel model, ObjectModelClassifier classifier) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_DAO_SUPER_CLASS, classifier, model);
        return value;
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_PERSISTENCE_CONTEXT_SUPER_CLASS}
     * tag value on the given model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_PERSISTENCE_CONTEXT_SUPER_CLASS
     * @since 1.0
     */
    public static String getPersistenceContextSuperClassTagValue(ObjectModel model) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_PERSISTENCE_CONTEXT_SUPER_CLASS, null, model);
        return value;
    }

    /**
     * Obtain the value of the {@link JpaTemplatesTagValues#TAG_INVERSE}
     * tag value on the given attribute.
     * <p/>
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see JpaTemplatesTagValues#TAG_INVERSE
     * @since 2.5
     */
    public static String getInverseTagValue(ObjectModelAttribute attribute) {
        String value = findTagValue(JpaTemplatesTagValues.TAG_INVERSE, attribute, null);
        return value;
    }

    /**
     * Check if the given attribute has the
     * {@link JpaTemplatesStereoTypes#STEREOTYPE_UNIQUE} stereotype.
     *
     * @param attribute attribute to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see JpaTemplatesStereoTypes#STEREOTYPE_UNIQUE
     * @since 2.5
     */
    public static boolean hasUniqueStereotype(ObjectModelAttribute attribute) {
        return attribute.hasStereotype(JpaTemplatesStereoTypes.STEREOTYPE_UNIQUE);
    }

    /**
     * Check if the given class has the
     * {@link JpaTemplatesStereoTypes#STEREOTYPE_NOT_GENERATED_VALUE} stereotype.
     *
     * @param clazz class to test
     * @return {@code true} if stereotype was found, {@code false otherwise}
     * @see JpaTemplatesStereoTypes#STEREOTYPE_NOT_GENERATED_VALUE
     * @since 2.5
     */
    public static boolean hasNotGeneratedValueStereotype(ObjectModelClassifier clazz) {
        return clazz.hasStereotype(JpaTemplatesStereoTypes.STEREOTYPE_NOT_GENERATED_VALUE);
    }

    public static boolean generateEntityGeneratedValueAnnotation(ObjectModel model,
                                                                 ObjectModelClass clazz) {
        boolean result = !useIdFactory(model);
        if (result) {

            // check entity is not marked to not generate value
            result = !hasNotGeneratedValueStereotype(clazz);
        }
        return result;
    }
}
