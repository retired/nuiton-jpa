package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import org.apache.commons.collections.CollectionUtils;
import org.nuiton.eugene.java.ObjectModelTransformerToJava;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * Common class for jpa like transformer.
 *
 * @author tchemit <chemit@codelutin.com>
 * @see JpaEntityTransformer
 * @since 1.0
 */
public abstract class AbstractJpaTransformer extends ObjectModelTransformerToJava {

    public static final String DEFAULT_CONSTANT_PREFIX = "PROPERTY_";

    protected void addConstructorWithEntityManager(ObjectModelClass output) {

        ObjectModelOperation constructor = addConstructor(
                output,
                ObjectModelJavaModifier.PUBLIC);

        addParameter(constructor, EntityManager.class, "entityManager");

        setOperationBody(constructor, ""
    /*{
        super(entityManager);
    }*/
        );
    }

    protected boolean isSuperClassEntity(ObjectModelClass input) {
        // test if a super class is in same package (so is another entity)
        boolean superClassIsBean = false;
        Collection<ObjectModelClass> superclasses = input.getSuperclasses();
        if (CollectionUtils.isNotEmpty(superclasses)) {
            for (ObjectModelClass superclass : superclasses) {
                if (input.getPackageName().equals(superclass.getPackageName())) {
                    superClassIsBean = true;
                    break;
                }
            }
        }
        return superClassIsBean;
    }

    protected boolean isEntity(ObjectModelClass input, String type) {

        ObjectModelClass aClass = getModel().getClass(type);

        // test if a super class is in same package (so is another entity)
        boolean result = aClass != null &&
                         input.getPackageName().equals(aClass.getPackageName());
        return result;
    }

    protected void createPropertyConstant(ObjectModelClass output,
                                          ObjectModelAttribute attr,
                                          String prefix,
                                          Set<String> constantNames) {

        String attrName = JpaTemplatesGeneratorUtil.getAttributeName(attr);

        String constantName = prefix + builder.getConstantName(attrName);

        if (!constantNames.contains(constantName)) {

            addConstant(output,
                        constantName,
                        String.class,
                        "\"" + attrName + "\"",
                        ObjectModelJavaModifier.PUBLIC
            );
        }
    }

    protected void addSerializable(ObjectModelClass input,
                                   ObjectModelClass output,
                                   boolean interfaceFound) {
        if (!interfaceFound) {
            addInterface(output, Serializable.class);
        }

        // Generate the serialVersionUID
        long serialVersionUID = JpaTemplatesGeneratorUtil.generateSerialVersionUID(input);

        addConstant(output,
                    JpaTemplatesGeneratorUtil.SERIAL_VERSION_UID,
                    "long",
                    serialVersionUID + "L",
                    ObjectModelJavaModifier.PRIVATE
        );
    }

    /**
     * Add all interfaces defines in input class and returns if
     * {@link Serializable} interface was found.
     *
     * @param input  the input model class to process
     * @param output the output generated class
     * @return {@code true} if {@link Serializable} was found from input,
     *         {@code false} otherwise
     */
    protected boolean addInterfaces(ObjectModelClass input,
                                    ObjectModelClass output) {
        boolean foundSerializable = false;
        for (ObjectModelInterface parentInterface : input.getInterfaces()) {
            String fqn = parentInterface.getQualifiedName();
            addInterface(output, fqn);
            if (Serializable.class.getName().equals(fqn)) {
                foundSerializable = true;
            }
        }
        return foundSerializable;
    }
}
