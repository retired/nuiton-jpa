package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.jpa.api.AbstractJpaPersistenceContext;
import org.nuiton.jpa.api.DefaultJpaEntityIdFactory;
import org.nuiton.jpa.api.JpaEntityIdFactory;

import javax.persistence.EntityManager;
import java.util.List;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * JpaPersistenceContextTransformer generates the persistence context for an model.
 * <ul>
 * <li>{@code AbstractXXXJpaPersistenceContext}: abstract jpa persistence context with all the technical stuff</li>
 * <li>{@code XXXJpaPersistenceContext}: concrete public jpa persistence context to use in your persistence layer</li>
 * </ul>
 * {@code Note:} All classes found in class-path are not generated.
 * <p/>
 * TODO Liste des entités (A voir meta-modèle...)
 *
 * @author tchemit <chemit@codelutin.com>
 * @plexus.component role="org.nuiton.eugene.Template" role-hint="org.nuiton.jpa.templates.JpaPersistenceContextTransformer"
 * @since 1.0
 */
public class JpaPersistenceContextTransformer extends AbstractJpaTransformer {

    @Override
    public void transformFromModel(ObjectModel model) {

        String packageName = JpaTemplatesGeneratorUtil.getPersistenceContextPackage(this, model);

        String entityAbstractName = JpaTemplatesGeneratorUtil.getPersistenceContextAbstractName(model);

        String entityConcreteName = JpaTemplatesGeneratorUtil.getPersistenceContextConcreteName(model);

        boolean generateAbstract = !isInClassPath(packageName, entityAbstractName);

        boolean generateConcrete = !isInClassPath(packageName, entityConcreteName);

        boolean useIdGenerator =
                JpaTemplatesGeneratorUtil.useIdFactory(getModel());

        if (generateAbstract) {

            generateAbstract(packageName,
                             entityAbstractName,
                             useIdGenerator);
        }

        if (generateConcrete) {

            generateImpl(packageName,
                         entityAbstractName,
                         entityConcreteName,
                         useIdGenerator);
        }
    }

    protected void generateAbstract(String packageName,
                                    String className,
                                    boolean useIdGenerator) {

        // try to find a super class by tag-value
        String superClass = JpaTemplatesGeneratorUtil.getPersistenceContextSuperClassTagValue(
                model);

        if (superClass == null) {

            // no super-class, use default one
            superClass = AbstractJpaPersistenceContext.class.getName();
        }

        ObjectModelClass output = createAbstractClass(className, packageName);

        setSuperClass(output, superClass);

        // detect if there is a contract to set on abstract
        String contractName =
                JpaTemplatesGeneratorUtil.getPersistenceContextInterfaceName(model);

        boolean addPersistenceContextContract = isInClassPath(packageName , contractName);

        if (addPersistenceContextContract) {
            addInterface(output, packageName + "." + contractName);
        }

        if (useIdGenerator) {

            // using an idGenerator

            addImport(output, DefaultJpaEntityIdFactory.class);
            ObjectModelOperation constructor = addConstructor(
                    output,
                    ObjectModelJavaModifier.PUBLIC);

            addParameter(constructor, EntityManager.class, "entityManager");
            setOperationBody(constructor, ""
    /*{
        super(new DefaultJpaEntityIdFactory(), entityManager);
    }*/
            );

            addConstructorWithIdGeneratorAndEntityManager(output);

        } else {

            // not using an idGenerator
            addConstructorWithEntityManager(output);
        }

        // add dao factories
        List<ObjectModelClass> entityClasses =
                JpaTemplatesGeneratorUtil.getEntityClasses(getModel(), true);

        for (ObjectModelClass aClass : entityClasses) {

            // add dao factory method
            String daoPackageName = JpaTemplatesGeneratorUtil.getDaoPackage(this, model, aClass);
            String daoConcreteName = JpaTemplatesGeneratorUtil.getDaoConcreteName(aClass);

            ObjectModelOperation operation = addOperation(
                    output,
                    "get" + aClass.getName() + "Dao",
                    daoPackageName + "." + daoConcreteName);
            setOperationBody(operation, ""
    /*{
        return new <%=daoConcreteName%>(entityManager);
    }*/
            );
        }

    }

    protected ObjectModelClass generateImpl(String packageName,
                                            String entityAbstractName,
                                            String entityConcreteName,
                                            boolean useIdGenerator) {

        ObjectModelClass output = createClass(entityConcreteName, packageName);

        setSuperClass(output, entityAbstractName);

        if (useIdGenerator) {

            addConstructorWithIdGeneratorAndEntityManager(output);
        }
        addConstructorWithEntityManager(output);

        return output;
    }

    protected void addConstructorWithIdGeneratorAndEntityManager(ObjectModelClass output) {

        ObjectModelOperation constructor = addConstructor(
                output,
                ObjectModelJavaModifier.PUBLIC);

        addParameter(constructor, JpaEntityIdFactory.class, "jpaEntityIdFactory");
        addParameter(constructor, EntityManager.class, "entityManager");

        setOperationBody(constructor, ""
    /*{
        super(jpaEntityIdFactory, entityManager);
    }*/
        );
    }
}
