package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.ModelPropertiesUtil;

/**
 * The JPA templates provider of tag values and stereotypes.
 *
 * @author tchemit <chemit@codelutin.com>
 * @plexus.component role="org.nuiton.eugene.ModelPropertiesUtil$ModelPropertiesProvider" role-hint="jpa"
 * @since 2.7
 */
public class JpaTemplatesModelPropertiesProvider extends ModelPropertiesUtil.ModelPropertiesProvider {

    @Override
    public void init() throws IllegalAccessException {
        scanStereotypeClass(JpaTemplatesStereoTypes.class);
        scanTagValueClass(JpaTemplatesTagValues.class);
    }
}
