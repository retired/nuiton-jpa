package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.EugeneTagValues;
import org.nuiton.eugene.ModelPropertiesUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClassifier;

/**
 * Defines all tag values managed by JPA templates.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public interface JpaTemplatesTagValues extends EugeneTagValues {

    /**
     * Tag value to use a super class for generated bean.
     *
     * @see JpaTemplatesGeneratorUtil#getEntitySuperClassTagValue(ObjectModel, ObjectModelClassifier)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(
            target = {ObjectModel.class, ObjectModelClassifier.class},
            documentation = "To specify a super-class to used on generated entities " +
                            "for a class or any class of a model")
    String TAG_ENTITY_SUPER_CLASS = "entitySuperClass";

    /**
     * Tag value to use a super class for generated bean.
     *
     * @see JpaTemplatesGeneratorUtil#getDaoSuperClassTagValue(ObjectModel, ObjectModelClassifier)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(
            target = {ObjectModel.class, ObjectModelClassifier.class},
            documentation = "To specify a super-class to used on generated dao " +
                            "for a class or any class of a model")
    String TAG_DAO_SUPER_CLASS = "daoSuperClass";

    /**
     * Tag value to use a super class for generated bean.
     *
     * @see JpaTemplatesGeneratorUtil#getDaoSuperClassTagValue(ObjectModel, ObjectModelClassifier)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(
            target = {ObjectModel.class},
            documentation = "To specify a super-class to used on generated persistence context")
    String TAG_PERSISTENCE_CONTEXT_SUPER_CLASS = "persistenceContextSuperClass";

    /**
     * Tag pour permettre de choisir qui contrôle la relation N-N
     * bidirectionnelle. On a inverse=true sur le père de la relation..
     * <p/>
     * Par défaut le inverse=true est placé sur le premier rôle trouvé dans
     * l'ordre alphabétique.
     *
     * @see JpaTemplatesGeneratorUtil#getInverseTagValue(ObjectModelAttribute)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(target = {ObjectModelAttribute.class},
                                            documentation = "Sets which part of a N-N relation is master (inverse=true) and slave (inverse=false) (must be put on each side on a such relation) (Hibernate mapping)")
    String TAG_INVERSE = "inverse";

    /**
     * Tag value to get user defined id factory.
     *
     * @see JpaTemplatesGeneratorUtil#getIdFactoryTagValue(ObjectModel)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(
            target = {ObjectModel.class},
            documentation = "To specify a user defined id factory to use for all entities of the model")
    String TAG_ID_FACTORY = "idFactory";

    /**
     * Tag value to generate property change listeners on entities
     * if setted to {@code true}.
     *
     * @see JpaTemplatesGeneratorUtil#getGeneratePropertyChangeListenersTagValue(ObjectModel)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(
            target = {ObjectModel.class},
            documentation = "Tag value to generate property change listeners on entities.")
    String TAG_GENERATE_PROPERTY_CHANGE_LISTENERS = "generatePropertyChangeListeners";

    /**
     * Tag value to generate visitors on entities if setted to {@code true}.
     *
     * @see JpaTemplatesGeneratorUtil#getGenerateVisitorsTagValue(ObjectModel)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(
            target = {ObjectModel.class},
            documentation = "Tag value to generate visitors on entities.")
    String TAG_GENERATE_VISITORS = "generateVisitors";

    /**
     * Tag value to generate extra technical fields (version, createDate) for
     * a specific entity or for all entities of a model.
     *
     * @see JpaTemplatesGeneratorUtil#getGenerateExtraTechnicalFieldsTagValue(ObjectModel, ObjectModelClassifier)
     * @since 1.0
     */
    @ModelPropertiesUtil.TagValueDefinition(
            target = {ObjectModel.class, ObjectModelClassifier.class},
            documentation = "Tag value to generate extra technical fields (version, createDate) for \n" +
                            "a specific entity or for all entities of a model.")
    String TAG_GENERATE_EXTRA_TECHNICAL_FIELDS = "generateExtraTechnicalFields";

}
