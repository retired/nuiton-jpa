package org.nuiton.jpa.templates;

/*
 * #%L
 * Nuiton Jpa :: Temlates
 * %%
 * Copyright (C) 2013 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.EugeneStereoTypes;
import org.nuiton.eugene.ModelPropertiesUtil;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClassifier;

import javax.persistence.GeneratedValue;

/**
 * Defines all stereotypes managed by JPA templates.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.0
 */
public interface JpaTemplatesStereoTypes extends EugeneStereoTypes {

    /**
     * Stéréotype pour les collections avec unicité.
     *
     * @see JpaTemplatesGeneratorUtil#hasUniqueStereotype(ObjectModelAttribute)
     * @since 1.0
     */
    @ModelPropertiesUtil.StereotypeDefinition(target = ObjectModelAttribute.class,
                                              documentation = "To specify that an attribute is unique (JPA mapping)")
    String STEREOTYPE_UNIQUE = "unique";

    /**
     * Stéréotype pour ne pas générer l'annotation {@link GeneratedValue} sur une entité.
     *
     * @see JpaTemplatesGeneratorUtil#hasNotGeneratedValueStereotype(ObjectModelClassifier)
     * @since 1.0
     */
    @ModelPropertiesUtil.StereotypeDefinition(target = ObjectModelClassifier.class,
                                              documentation = "To specify that an entity deal by itself with his id, the GeneratedValue annotation will not be generated then(JPA mapping)")
    String STEREOTYPE_NOT_GENERATED_VALUE = "notGeneratedValue";
}
