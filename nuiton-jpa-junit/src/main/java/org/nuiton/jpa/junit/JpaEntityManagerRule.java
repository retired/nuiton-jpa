package org.nuiton.jpa.junit;

/*
 * #%L
 * Nuiton Jpa :: Junit
 * %%
 * Copyright (C) 2013 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.nuiton.jpa.hibernate.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class JpaEntityManagerRule implements TestRule {

    private static final Log log = LogFactory.getLog(JpaEntityManagerRule.class);

    protected String persistenceUnitName;

    protected static String timestamp = String.valueOf(new Date().getTime());

    protected boolean open = false;

    protected Map<String, String> jpaParameters;

    protected EntityManagerFactory entityManagerFactory;

    protected List<EntityManager> openedEntityManagers = new LinkedList<EntityManager>();

    public JpaEntityManagerRule(String persistenceUnitName, Map<String, String> jpaParameters) {
        this.persistenceUnitName = persistenceUnitName;
        this.jpaParameters = jpaParameters;
    }

    @Override
    public Statement apply(final Statement base, Description description) {

        final String testClassName = description.getClassName();

        final String testMethodName = description.getMethodName();

        if (log.isDebugEnabled()) {
            log.debug("will create entityManager for test class "
                      + testClassName + " and method " + testMethodName);
        }

        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                createEntityManagerFactory(testClassName, testMethodName);
                try {
                    base.evaluate();
                } finally {
                    closeEntityManager();
                }
            }
        };
    }

    protected void createEntityManagerFactory(String testClassName, String testMethodName) {

        String context = testClassName
                         + '_' + testMethodName
                         + '_' + timestamp;

        entityManagerFactory = HibernateUtil.createTempEntityManagerFactory(
                        persistenceUnitName, context, jpaParameters);

        if (log.isDebugEnabled()) {
            log.debug("created entityManagerFactory " + entityManagerFactory);
        }

        open = true;

    }

    /**
     * @deprecated use {@link #newEntityManager()} and save a reference to it
     */
    public EntityManager getEntityManager() {

        if (openedEntityManagers.isEmpty()) {

            openedEntityManagers.add(newEntityManager());

        }

        return openedEntityManagers.get(0);

    }

    public EntityManager newEntityManager() {

        if ( ! open) {
            throw new IllegalStateException("entity manager is not yet opened");
        }

        EntityManager entityManager =
                entityManagerFactory.createEntityManager();

        openedEntityManagers.add(entityManager);

        return entityManager;

    }

    /** Override to tear down your specific external resource. */
    protected void closeEntityManager() {

        open = false;

        for (EntityManager openedEntityManager : openedEntityManagers) {

            if (log.isDebugEnabled()) {
                log.debug("close entityManager " + openedEntityManager);
            }

            openedEntityManager.close();

        }

    }

}
